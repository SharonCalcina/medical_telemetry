import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:medical_telemetry/menu.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool _inAsyncCall = false;
  int genderSel = 1;
  int selected = 2;
  int currentIndex = 3;
  bool ini = true;
  bool create = false;
  bool showPassword = true;
  bool checkBox = false;
  bool button = true;
  String value;
  final _formIni = GlobalKey<FormState>();
  final _formCreate = GlobalKey<FormState>();
  TextEditingController mail = TextEditingController();
  TextEditingController password = TextEditingController();

  TextEditingController genero = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController contrasena = TextEditingController();
  TextEditingController especialidad = TextEditingController();
  TextEditingController nombres = TextEditingController();
  TextEditingController apellidos = TextEditingController();
  TextEditingController edad = TextEditingController();
  String especialidadDoc = "";
  String generoDoc = "F";
  String message = "";
  String messagelog = "";
  List<String> especialidades = [
    "Medicina General",
    "Oncología",
    " Cardiología",
    "Pediatría",
    "Gatroenterología"
  ];

  setSelected(int val) {
    setState(() {
      selected = val;
    });
  }

  Future sendDoctor() async {
    final response = await http.post(
        Uri.parse('https://teracorp.ga/doctor_registration.php'
            //"http://18.230.28.95/e-health/doctor_registration.php"
            ),
        body: {
          'd_full_name': "${nombres.value.text}",
          'd_last_name': "${apellidos.value.text}",
          'd_sex': generoDoc,
          'd_age': "${edad.value.text}",
          'd_speciality': especialidadDoc,
          'd_email': "${correo.value.text}",
          'd_password': "${contrasena.value.text}"
        });
    var dataUser = jsonDecode(response.body);
    List<String> resp = dataUser.toString().split(",");
    message = resp[1].replaceAll("}", "");
  }

  Future logDoctor() async {
    final response = await http.post(
        Uri.parse('https://teracorp.ga/doctor_login.php'
            //'http://192.168.0.16/doctor_login.php'
            //"http://18.230.28.95/e-health/doctor_registration.php"
            ),
        body: {
          'd_password': "${password.value.text}",
          'd_email': "${mail.value.text}",
        });
    var dataUser = jsonDecode(response.body);
    List<String> resp = dataUser.toString().split(",");
    messagelog = resp[resp.length - 1].replaceAll("}", "");
    //print("meeeeeeeeeeeeeeeeeeeeeeeeeeeeeesssssssssssss$messagelog");
  }

  String kk = "";
  Future<String> docId() async {
    print(mail.value.text);
    final response = await http
        .post(Uri.parse('https://teracorp.ga/doctor_id.php'), body: {});
    var dataUser = jsonDecode(response.body);
    String resp = dataUser.toString();
    print(resp + "ppppppppppppppppppppp");
    setState(() {
      kk = resp
              .toString()
              .split('${mail.value.text}')[1]
              .toString()
              .split(" ")[2]
              .replaceAll("},", "")
              .toString()
          //.split(" ").toString();
          //[1].replaceAll("}]", "")
          ;
    });
    print(kk + "ppppppppppppppppppppp");
    //print(
    //    kk.toString().split(" ")[1].replaceAll("}]", "") + "*****************");
    return kk;
  }

  @override
  void initState() {
    // TODO: implement initState
    mail.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.9),
        body: Stack(
          children: [
            Image.asset(
              "assets/images/m.jpg",
              fit: BoxFit.cover,
              height: height,
              width: width,
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 80,
                  ),
                  Text(
                    "Bienvenido",
                    style: TextStyle(
                      color: Colors.white, fontSize: 16,
                      // fontWeight: FontWeight.bold
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(25),
                    //width: MediaQuery.of(context).size.width/1.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.5),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(children: <Widget>[
                          Radio(
                            activeColor: Colors.black,
                            value: 1,
                            groupValue: selected,
                            onChanged: (val) {
                              ini = false;
                              create = true;
                              setSelected(val);
                            },
                          ),
                          Text(
                            "Crear cuenta",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                          ),
                        ]),
                        Form(
                            key: _formCreate,
                            child: Visibility(
                                visible: create,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: nombres,
                                        decoration: InputDecoration(
                                            fillColor: Colors.red,
                                            prefixIcon: Icon(
                                              Icons.person,
                                              color: Colors.white
                                                  .withOpacity(0.45),
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Nombres",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        textAlign: TextAlign.center,
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: apellidos,
                                        decoration: InputDecoration(
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Apellidos",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),
                                    Container(
                                        padding:
                                            EdgeInsets.only(left: 8, right: 10),
                                        margin: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.85),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Row(
                                          children: [
                                            Theme(
                                              data: ThemeData(
                                                //here change to your color
                                                unselectedWidgetColor:
                                                    Colors.white,
                                              ),
                                              child: Radio(
                                                focusColor: Colors.white,
                                                hoverColor: Colors.white,
                                                value: 1,
                                                activeColor: Colors.amber,
                                                onChanged: (val) {
                                                  setState(() {
                                                    genderSel = val;
                                                    generoDoc = "F";
                                                  });
                                                },
                                                groupValue: genderSel,
                                              ),
                                            ),
                                            text(
                                                "Femenino",
                                                12.0,
                                                Colors.white.withOpacity(0.6),
                                                FontWeight.normal),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Theme(
                                                data: ThemeData(
                                                    unselectedWidgetColor:
                                                        Colors.white),
                                                child: Radio(
                                                  value: 2,
                                                  activeColor: Colors.amber,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      genderSel = val;
                                                      generoDoc = "M";
                                                    });
                                                  },
                                                  groupValue: genderSel,
                                                )),
                                            text(
                                                "Masculino",
                                                12.0,
                                                Colors.white.withOpacity(0.6),
                                                FontWeight.normal),
                                          ],
                                        )),
                                    /*Container(
                                        padding:
                                            EdgeInsets.only(left: 8, right: 10),
                                        margin: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.85),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: TextFormField(
                                          validator: (m) {
                                            return (m.isEmpty)
                                                ? 'Error!. Intenta de nuevo'
                                                : null;
                                          },
                                          decoration: InputDecoration(
                                              prefixIcon: Icon(
                                                Icons.emoji_people,
                                                color: Colors.white
                                                    .withOpacity(0.45),
                                              ),
                                              errorStyle: TextStyle(
                                                  fontSize: 12,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.red.shade400),
                                              hintText: "Género  (F  o  M)",
                                              hintStyle: TextStyle(
                                                  color: Colors.white
                                                      .withOpacity(0.6),
                                                  fontSize: 14),
                                              border: InputBorder.none),
                                          autofocus: false,
                                          controller: genero,
                                          cursorColor: Colors.amber,
                                          autocorrect: true,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                          ),
                                        )),*/
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: edad,
                                        decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.cake,
                                              color: Colors.white
                                                  .withOpacity(0.45),
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Edad",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 5, bottom: 5),
                                        padding: EdgeInsets.only(
                                            left: 20, right: 10),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.85),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Row(
                                          children: [
                                            DropdownButton(
                                                dropdownColor:
                                                    Colors.blueGrey.shade900,
                                                iconDisabledColor: Colors.white
                                                    .withOpacity(0.4),
                                                iconEnabledColor: Colors.amber,
                                                itemHeight: 50.0,
                                                icon: Icon(
                                                  Icons.work,
                                                ),
                                                hint: text(
                                                    "  Selecciona tu especialidad      ",
                                                    14.0,
                                                    Colors.white
                                                        .withOpacity(0.6),
                                                    FontWeight.normal),
                                                onChanged: (val) {
                                                  setState(() {
                                                    value = val;
                                                    especialidadDoc = val;
                                                  });
                                                },
                                                value: value,
                                                items: <String>[
                                                  "Anestesiología",
                                                  "Cardiología",
                                                  "Cirujía General",
                                                  "Dermatología",
                                                  "Gatroenterología",
                                                  "Medicina General",
                                                  "Oncología",
                                                  "Oftalmología",
                                                  "Otorrinolarngología",
                                                  "Pediatría",
                                                  "Traumatología",
                                                  "Ulrología",
                                                  "Otro"
                                                ].map<DropdownMenuItem<String>>(
                                                    (String value) {
                                                  return DropdownMenuItem(
                                                    child: text(
                                                        value,
                                                        12.0,
                                                        Colors.white,
                                                        FontWeight.normal),
                                                    value: value,
                                                  );
                                                }).toList())
                                          ],
                                        )),

                                    /* Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: especialidad,
                                        decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.work,
                                              color: Colors.white
                                                  .withOpacity(0.45),
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Especialidad",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),*/
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: correo,
                                        decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.mail,
                                              color: Colors.white
                                                  .withOpacity(0.45),
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "correo electrónico",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.85),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                        autofocus: false,
                                        controller: contrasena,
                                        decoration: InputDecoration(
                                            fillColor: Colors.white,
                                            prefixIcon: Icon(
                                              Icons.lock_rounded,
                                              color: Colors.white
                                                  .withOpacity(0.45),
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade400,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Contraseña",
                                            hintStyle: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.6),
                                                fontSize: 14)),
                                        cursorColor: Colors.amber,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ElevatedButton(
                                        child: Text(
                                          "Registrar",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black),
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          //padding: EdgeInsets.all(12),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          elevation: 5,
                                          primary: Colors.cyan.shade200,
                                        ),
                                        onPressed: () async {
                                          if (_formCreate.currentState
                                                  .validate() &&
                                              generoDoc != "" &&
                                              especialidadDoc != "") {
                                            if (!correo.text
                                                .toString()
                                                .isValidEmail()) {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext
                                                          context) =>
                                                      AlertDialog(
                                                          backgroundColor:
                                                              Colors.grey
                                                                  .shade400,
                                                          title: Text(
                                                            "Error",
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .red
                                                                    .shade800,
                                                                fontSize: 16),
                                                          ),
                                                          content: Text(
                                                            "El correo esta mal formateado",
                                                            style: TextStyle(
                                                                fontSize: 14),
                                                          )));
                                            } else {
                                              setState(() {
                                                _inAsyncCall = true;
                                              });

                                              sendDoctor();
                                              Timer(Duration(seconds: 3), () {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        backgroundColor: Colors
                                                            .grey.shade400,
                                                        title: Text(
                                                          "Mensaje de Registro",
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .red.shade800,
                                                              fontSize: 16),
                                                        ),
                                                        content: Text(message),
                                                      );
                                                    });
                                                setState(() {
                                                  nombres.clear();
                                                  apellidos.clear();
                                                  edad.clear();
                                                  correo.clear();
                                                  contrasena.clear();
                                                  especialidadDoc = "";
                                                  generoDoc = "F";
                                                  genderSel = 1;
                                                  _inAsyncCall = false;
                                                });
                                              });
                                            }
                                          } else {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) =>
                                                        AlertDialog(
                                                            backgroundColor:
                                                                Colors.grey
                                                                    .shade400,
                                                            title: Text(
                                                              "Error",
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .red
                                                                      .shade800,
                                                                  fontSize: 16),
                                                            ),
                                                            content: Text(
                                                              "Rellene todos los campos del formulario",
                                                              style: TextStyle(
                                                                  fontSize: 14),
                                                            )));
                                          }
                                        }),
                                  ],
                                ))),
                        Row(children: <Widget>[
                          Radio(
                            activeColor: Colors.black,
                            value: 2,
                            groupValue: selected,
                            onChanged: (val) {
                              setSelected(val);
                              ini = true;
                              create = false;
                            },
                          ),
                          Text(
                            "Iniciar sesion",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ]),
                        Form(
                            key: _formIni,
                            child: Visibility(
                                visible: ini,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10),
                                        margin: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: TextFormField(
                                          validator: (m) {
                                            return (m.isEmpty)
                                                ? 'Error!.Intenta de nuevo'
                                                : null;
                                          },
                                          decoration: InputDecoration(
                                              errorStyle: TextStyle(
                                                  fontSize: 12,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.red.shade900),
                                              hintText: "Ingresa tu correo",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey.shade600,
                                                  fontSize: 14),
                                              border: InputBorder.none),
                                          autofocus: false,
                                          controller: mail,
                                          cursorColor: Colors.black,
                                          autocorrect: true,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                        )),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5, right: 5, bottom: 5),
                                      padding:
                                          EdgeInsets.only(left: 5, right: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: TextFormField(
                                        obscureText: showPassword,
                                        validator: (p) {
                                          return (p.isEmpty)
                                              ? 'Error!.Intenta de nuevo'
                                              : null;
                                          //return 'prosigue';
                                        },
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14),
                                        autofocus: false,
                                        controller: password,
                                        decoration: InputDecoration(
                                            errorStyle: TextStyle(
                                                color: Colors.red.shade900,
                                                fontSize: 12,
                                                fontStyle: FontStyle.italic),
                                            border: InputBorder.none,
                                            hintText: "Ingresa tu contraseña",
                                            hintStyle: TextStyle(
                                                color: Colors.grey.shade600,
                                                fontSize: 14)),
                                        cursorColor: Colors.black,
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Checkbox(
                                              activeColor: Colors.white,
                                              checkColor: Colors.blue,
                                              value: checkBox,
                                              onChanged: (val) {
                                                setState(() {
                                                  showPassword = !showPassword;
                                                  checkBox = val;
                                                });
                                              }),
                                          Text(
                                            "Mostrar contraseña",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      ),
                                    ),
                                    RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      splashColor:
                                          Colors.white.withOpacity(0.8),
                                      onPressed: () async {
                                        if (_formIni.currentState.validate()) {
                                          setState(() {
                                            _inAsyncCall = true;
                                          });
                                          logDoctor();
                                          Timer(Duration(seconds: 3), () async {
                                            if (messagelog.contains(
                                                "Se ha logueado satisfactoriamente")) {
                                              String id = await docId();
                                              print(id + "==============");
                                              _inAsyncCall = false;
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(builder:
                                                      (BuildContext context) {
                                                return Menu(id);
                                              }));
                                            } else {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext
                                                          context) =>
                                                      AlertDialog(
                                                          backgroundColor:
                                                              Colors.grey
                                                                  .shade400,
                                                          title: Text(
                                                            "Error",
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .red
                                                                    .shade800,
                                                                fontSize: 16),
                                                          ),
                                                          content: Text(
                                                              messagelog,
                                                              //"El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                              textAlign:
                                                                  TextAlign
                                                                      .justify,
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      14))));
                                              setState(() {
                                                mail.clear();
                                                password.clear();
                                                _inAsyncCall = false;
                                              });
                                            }
                                          });
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                      backgroundColor:
                                                          Colors.grey.shade400,
                                                      title: Text(
                                                        "Error",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors
                                                                .red.shade800,
                                                            fontSize: 16),
                                                      ),
                                                      content: Text(
                                                          "Rellene todos los campos",
                                                          //"El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                          textAlign:
                                                              TextAlign.justify,
                                                          style: TextStyle(
                                                              fontSize: 14))));
                                        }
                                      },
                                      textColor: Colors.black,
                                      elevation: 5,
                                      child: Text(
                                        "Continuar",
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      color: Colors.cyan.shade200,
                                    )
                                  ],
                                )))
                      ],
                    ),
                  ),
                  /* TextButton(
                      onPressed: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Menu();
                        }));
                      },
                      child: Text("Ir a Menu"))*/
                ],
              ),
            ),
            (_inAsyncCall)
                ? Container(
                    decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.6),
                    ),
                    width: double.infinity,
                    height: double.infinity,
                    child: Center(child: CircularProgressIndicator()))
                : Container()
          ],
        ));
  }

  Widget userContent(icono, String titulo, pagetogo) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return pagetogo;
          }));
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(15),
          child: Row(
            children: [
              Icon(icono),
              SizedBox(
                width: 20,
              ),
              Text(titulo)
            ],
          ),
        ));
  }

  Widget text(title, size, color, font) {
    return Text(
      title,
      textAlign: TextAlign.center,
      maxLines: 2,
      style: TextStyle(
          color: color, fontSize: size, fontWeight: font, fontFamily: "Kanit"),
    );
  }

  Widget msge() {
    return Text(
      'Error!. Intenta de nuevo',
      style: TextStyle(fontSize: 12),
    );
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
