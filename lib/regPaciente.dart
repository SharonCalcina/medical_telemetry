import 'dart:convert';
import 'package:barcode_scan_fix/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'menu.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class RegPaciente extends StatefulWidget {
  final String idDoc;
  RegPaciente(this.idDoc);
  @override
  _RegPacienteState createState() => _RegPacienteState(this.idDoc);
}

class _RegPacienteState extends State<RegPaciente> {
  _RegPacienteState(this.idDoc);
  String idDoc;
  String generoPac = "F";
  int genderSel = 1;
  int selected = 2;
  int currentIndex = 3;
  bool ini = true;
  bool create = false;
  bool showPassword = true;
  bool checkBox = false;
  bool button = true;

  final _formCreate = GlobalKey<FormState>();
  String fecha = DateFormat.yMMMd().format(DateTime.now()).toString();
  // TextEditingController idManilla = TextEditingController();
  TextEditingController nombre = TextEditingController();
  TextEditingController genero = TextEditingController();
  TextEditingController edad = TextEditingController();
  TextEditingController fechareg = TextEditingController();
  bool _inAsyncCall = false;
  String message = "";
  int camera = 1;
  Future sendPaciente() async {
    final response = await http
        .post(Uri.parse("https://teracorp.ga/patient_registration.php"), body: {
      'p_patient_number': "$barcode",
      'p_full_name': "${nombre.value.text.toUpperCase()}",
      'p_sex': generoPac,
      'p_age': "${edad.value.text}",
      'doctor_id': idDoc
    });
    var dataUser = jsonDecode(response.body);

    //print("meeeeeeeeeeeeeeeeeeeeeeeeeeeeeesssssssssssss${dataUser.toString()}");

    List<String> resp = dataUser.toString().split(",");

    message = resp[1].replaceAll("}", "");
  }

  String barcode = "";

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.9),
        body: Stack(
          children: [
            Image.asset(
              "assets/images/regPac.jpg",
              fit: BoxFit.cover,
              height: height,
              width: width,
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 80,
                  ),
                  Text(
                    "Registro de Pacientes",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(25),
                    //width: MediaQuery.of(context).size.width/1.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.5),
                    ),
                    child: Column(
                      children: <Widget>[
                        Form(
                            key: _formCreate,
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () async {
                                      String codeSanner = await BarcodeScanner
                                          .scan(); //barcode scnner
                                      setState(() {
                                        barcode = codeSanner;
                                        print(barcode);
                                      });
                                    },
                                    child: Container(
                                        height: 45,
                                        width: double.infinity,
                                        margin: EdgeInsets.only(
                                            left: 5, right: 5, bottom: 5),
                                        padding: EdgeInsets.only(
                                            left: 5, right: 10, bottom: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.85),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: 12,
                                            ),
                                            Icon(
                                              Icons.qr_code_scanner,
                                              color: Colors.amber,
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Expanded(
                                                child: Text(
                                              "ID Manilla:  $barcode",
                                              style: TextStyle(
                                                  color: Colors.white
                                                      .withOpacity(0.8)),
                                            ))
                                          ],
                                        ))),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 5, right: 5, bottom: 5),
                                  padding: EdgeInsets.only(left: 5, right: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.85),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: TextFormField(
                                    validator: (p) {
                                      return (p.isEmpty)
                                          ? 'Error!.Intenta de nuevo'
                                          : null;
                                    },
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                    autofocus: false,
                                    controller: nombre,
                                    decoration: InputDecoration(
                                        prefixIcon: Icon(
                                          Icons.person,
                                          color: Colors.white.withOpacity(0.45),
                                        ),
                                        errorStyle: TextStyle(
                                            color: Colors.red.shade400,
                                            fontSize: 12,
                                            fontStyle: FontStyle.italic),
                                        border: InputBorder.none,
                                        hintText: "Nombre completo",
                                        hintStyle: TextStyle(
                                            color:
                                                Colors.white.withOpacity(0.6),
                                            fontSize: 14)),
                                    cursorColor: Colors.amber,
                                  ),
                                ),
                                Container(
                                    padding:
                                        EdgeInsets.only(left: 8, right: 10),
                                    margin: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.85),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Row(
                                      children: [
                                        Theme(
                                          data: ThemeData(
                                            //here change to your color
                                            unselectedWidgetColor: Colors.white,
                                          ),
                                          child: Radio(
                                            focusColor: Colors.white,
                                            hoverColor: Colors.white,
                                            value: 1,
                                            activeColor: Colors.amber,
                                            onChanged: (val) {
                                              setState(() {
                                                genderSel = val;
                                                generoPac = "F";
                                              });
                                            },
                                            groupValue: genderSel,
                                          ),
                                        ),
                                        text(
                                            "Femenino",
                                            12.0,
                                            Colors.white.withOpacity(0.6),
                                            FontWeight.normal),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Theme(
                                            data: ThemeData(
                                                unselectedWidgetColor:
                                                    Colors.white),
                                            child: Radio(
                                              value: 2,
                                              activeColor: Colors.amber,
                                              onChanged: (val) {
                                                setState(() {
                                                  genderSel = val;
                                                  generoPac = "M";
                                                });
                                              },
                                              groupValue: genderSel,
                                            )),
                                        text(
                                            "Masculino",
                                            12.0,
                                            Colors.white.withOpacity(0.6),
                                            FontWeight.normal),
                                      ],
                                    )),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 5, right: 5, bottom: 5),
                                  padding: EdgeInsets.only(left: 5, right: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.85),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: TextFormField(
                                    validator: (p) {
                                      return (p.isEmpty)
                                          ? 'Error!.Intenta de nuevo'
                                          : null;
                                    },
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                    autofocus: false,
                                    controller: edad,
                                    decoration: InputDecoration(
                                        prefixIcon: Icon(
                                          Icons.cake,
                                          color: Colors.white.withOpacity(0.45),
                                        ),
                                        errorStyle: TextStyle(
                                            color: Colors.red.shade400,
                                            fontSize: 12,
                                            fontStyle: FontStyle.italic),
                                        border: InputBorder.none,
                                        hintText: "Edad",
                                        hintStyle: TextStyle(
                                            color:
                                                Colors.white.withOpacity(0.6),
                                            fontSize: 14)),
                                    cursorColor: Colors.amber,
                                  ),
                                ),
                                Container(
                                    width: double.infinity,
                                    height: 45,
                                    margin: EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    padding: EdgeInsets.only(
                                        left: 15, right: 10, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.85),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.date_range,
                                          color: Colors.white.withOpacity(0.6),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Text("Fecha de registro:  $fecha",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14)),
                                      ],
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                ElevatedButton(
                                    child: Text(
                                      "Registrar",
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.black),
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      //padding: EdgeInsets.all(12),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      elevation: 5,
                                      primary: Colors.cyan.shade200,
                                    ),
                                    onPressed: () async {
                                      if (_formCreate.currentState.validate() &&
                                          barcode != "" &&
                                          generoPac != "") {
                                        setState(() {
                                          _inAsyncCall = true;
                                        });

                                        sendPaciente();

                                        Timer(Duration(seconds: 3), () {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                      backgroundColor:
                                                          Colors.grey.shade400,
                                                      title: Text(
                                                        "Mensaje de registro",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors
                                                                .red.shade800,
                                                            fontSize: 16),
                                                      ),
                                                      content: Text(message,
                                                          //"El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                          textAlign:
                                                              TextAlign.justify,
                                                          style: TextStyle(
                                                              fontSize: 14))));
                                          setState(() {
                                            barcode = "";
                                            nombre.clear();
                                            generoPac = "F";
                                            edad.clear();
                                            genderSel = 1;
                                          });

                                          _inAsyncCall = false;
                                        });
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) =>
                                                AlertDialog(
                                                    backgroundColor:
                                                        Colors.grey.shade400,
                                                    title: Text(
                                                      "Error",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          color: Colors
                                                              .red.shade800,
                                                          fontSize: 16),
                                                    ),
                                                    content: Text(
                                                        "Rellene todos los campos",
                                                        //"El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                        textAlign:
                                                            TextAlign.justify,
                                                        style: TextStyle(
                                                            fontSize: 14))));
                                      }
                                    })
                              ],
                            )),
                      ],
                    ),
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Menu(idDoc);
                        }));
                      },
                      child: text("Ir a Menú de inicio", 15.0, Colors.black,
                          FontWeight.w600))
                ],
              ),
            ),
            (_inAsyncCall)
                ? Container(
                    decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.6),
                    ),
                    width: double.infinity,
                    height: double.infinity,
                    child: Center(child: CircularProgressIndicator()))
                : Container()
          ],
        ));
  }

  Widget userContent(icono, String titulo, pagetogo) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return pagetogo;
          }));
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(15),
          child: Row(
            children: [
              Icon(icono),
              SizedBox(
                width: 20,
              ),
              Text(titulo)
            ],
          ),
        ));
  }

  Widget text(title, size, color, font) {
    return Text(
      title,
      textAlign: TextAlign.center,
      maxLines: 2,
      style: TextStyle(
          color: color, fontSize: size, fontWeight: font, fontFamily: "Kanit"),
    );
  }

  Widget msge() {
    return Text(
      'Error!. Intenta de nuevo',
      style: TextStyle(fontSize: 12),
    );
  }
}
